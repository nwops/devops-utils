# Hacks and scripts

## Port forwarder

Uses bolt inventory files to establish ssh tunnels that are protected by 2FA. This is meant to keep each tunnel open for 10-30 minutes. Once the script is run another script will create a dynamic inventory file based on tunnel map for bolt to use.

This is a WIP!

Usage:

1. cd 2fa_proxy
2. bundle install
3. bundle exec ./port_forwarder.rb

```
bundle exec ./port_forwarder.rb
What is your username? daffy
What is your 2fa passcode? Hurry!:  •••••
ssh -nNT -L 21489:localhost:22 daffy@192.168.20.109
ssh -nNT -L 24528:localhost:22 daffy@192.168.20.148
ssh -nNT -L 30526:localhost:22 daffy@192.168.20.146
ssh -nNT -L 22500:localhost:22 daffy@192.168.20.120
ssh -nNT -L 17525:localhost:22 daffy@192.168.20.145
ssh -nNT -L 19499:localhost:22 daffy@192.168.20.119
```
