#!/usr/bin/env ruby
require 'socket'
require 'bolt'
require 'bolt/config'
require 'bolt/inventory'
require 'bolt/logger'
require "tty-prompt"

prompt = TTY::Prompt.new

def initialize_bolt
  Bolt::Logger.initialize_logging
  @logger = Logging.logger[self]
  @config = Bolt::Config.default
  # Bolt::Boltdir.find_boltdir(Dir.pwd)
  # Bolt::Config.from_boltdir(boltdir, options)
  @inventory ||= Bolt::Inventory.from_config(@config, nil)
  @options = {}
end

def get_ip(name)
  name
end

def get_hotname(ip)
  ip
end

def get_port(ip)
  ip.split('.').reduce(0) {|acc, num| acc + num.to_i } + (1000 * Random.rand(2..31))
end

def proxy_host
  'localhost'
end

def port_map
  @port_map ||= begin
    m = {}
    @inventory.node_names.each do |name|
      p = get_port(name)
      m[name] = { port: p}
    end
    m
  end
end

initialize_bolt

user = prompt.ask('What is your username?', default: ENV['USER'])
code = prompt.mask("What is your 2fa passcode? Hurry!: ")
port_map.each do |name, config|
  puts "ssh -nNT -L #{config[:port]}:#{proxy_host}:22 #{user}@#{name}"
end

#https://stackoverflow.com/questions/23115619/how-to-use-ansible-with-two-factor-authentication

