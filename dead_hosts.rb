#!/opt/puppetlabs/puppet/bin/ruby
# frozen_string_literal: true

require 'resolv'
require 'puppet'
require 'json'
require 'puppet/face'
require 'highline/import'

MAX_THREAD_COUNT = 20
Puppet.initialize_settings # required, otherwise grabbing the settings doesn't work
CA_DIR = Puppet.settings[:cadir]
SIGNED_DIR = File.join(CA_DIR, 'signed')
PE_LIB = '/opt/puppetlabs/puppet/modules/puppet_enterprise/lib'
$LOAD_PATH << PE_LIB unless $LOAD_PATH.include?(PE_LIB)
NODE_FACE = Puppet::Face[:node, :current]

# @return a logger object
def logger
  @logger ||= begin
    require 'logger'
    level = if ENV['ENABLE_LOGGER']
              Logger::DEBUG
            else
              Logger::INFO
            end
    @logger = Logger.new(STDERR)
    @logger.level = level
    @logger
  end
end

def puppetdb_query(query)
  out = "curl -s -X GET https://#{Puppet.settings[:server]}:8081/pdb/query/v4 \
  --tlsv1 \
  --cacert #{Puppet.settings[:cacert]} \
  --cert #{Puppet.settings[:hostcert]} \
  --key #{Puppet.settings[:hostprivkey]} \
  --data-urlencode \"query=#{query}\""
  `#{out}`
end

# @return [Hash] - a list of node certnames as a hash
def node_query(query)
  data = JSON.parse(puppetdb_query(query))
  data.each_with_object({}) do |node, list|
    list[node['certname']] = node['value']
  end
end

# @return [Hash] - a certname to hostname Hash map of all the nodes
def cert_host_map
  @cert_host_map ||= node_query("facts {name = 'fqdn' }")
end

def purge_node(node_certs)
  # The deactivate action should only be available on master nodes as it is
  # included in the puppetdb-terminus package.
  if Puppet::Interface.find_action(:node, :deactivate, :current)
    NODE_FACE.deactivate(*node_certs)
    NODE_FACE.clean(*node_certs)
  else
    raise 'This command cannot be run on an agent node. Please try again on a master node.'
  end
end

# @summary - purges all passed in nodes from puppetdb and their certs
# param nodes [Array] - one or more hosts to purge
def purge_nodes(nodes, validate_each = false)
  # make purging the wrong node painfully hard
  if validate_each
    nodes.each do |node|
      answer = agree("<%= color('Purge: #{node}(y/n)? ', RED, BOLD) %>", default: false)
      purge_node(node) if answer || ENV['AUTO_PURGE'] == 'true'
    end
  else
    logger.warn('Purging all nodes in list')
    purge_node(nodes)
  end
end

# @return [Array] list of signed cert names
# @example
#  signed_certs => ['pe-xl-core0.puppet.vm.pem']
def signed_certs
  @signed_certs ||= Dir.glob(File.join(SIGNED_DIR, '*.pem'))
end

# @return [Array] list of signed cert names for nodes
# @example
#  signed_certs => ['pe-xl-core0.puppet.vm.pem']
def node_certs
  @node_certsw ||= signed_certs.reject do |cert|
    protected_certs.include?(cert)
  end
end

def infra_nodes
  data = puppetdb_query("resources[parameters] { type = 'Class' and title = 'Puppet_enterprise'}")
  params = JSON.parse(data)[0]['parameters']
  params.fetch_values('puppetdb_database_host', 'certificate_authority_host', 'console_database_host',
                      'console_host', 'database_host', 'puppetdb_host', 'puppet_master_host',
                      'pcp_broker_host', 'orchestrator_database_host',
                      'inventory_database_host', 'ha_enabled_replicas').flatten.uniq
end

def protected_certs
  @protected_certs ||= ['console-cert'] + infra_nodes
end

def deactivated_nodes
  node_query('nodes[certname] {deactivated is not null }')
end

def expired_nodes
  node_query('nodes[certname] {expired is not null }')
end

# @return [String] - the value of the node ttl
# @note calls out to puppetdb and checks the node_ttl from the class resource
def node_ttl
  query = "resources[parameters] {type = 'Class' and title = 'Puppet_enterprise::Profile::Puppetdb' }"
  data = JSON.parse(puppetdb_query(query))
  data.first['parameters']['node_ttl']
end

# @summary returns The current thread count that is currently active
#  a status of false or nil means the thread completed
#  so when anything else we count that as a active thread
# @return [Integer] - current thread count
def current_thread_count(items)
  active_threads = items.find_all do |_item, opts|
    if opts[:thread]
      opts[:thread].status
    else
      false
    end
  end
  logger.debug "Current thread count #{active_threads.count}"
  active_threads.count
end

# @return [Hash] a hash of all the hosts where the keys are the hostname
#   removes the .pem from the signed_cert filename
# @example
#   signed_hosts => { "hostname12345.vm" => { :hostname => "hostname12345.vm"}}
def signed_hosts
  @signed_hosts ||= begin
    signed_certs.each_with_object({}) do |file, list|
      hostname = File.basename(file, File.extname(file))
      list[hostname] = { hostname: hostname }
    end
  end
end

# @summary Set a limit on the amount threads used, defaults to 10
#   MAX_THREAD_COUNT can be used to set this limit
# @return [Integer] - returns the max_thread_count
def max_thread_limit
  @max_thread_limit ||= (ENV['MAX_THREAD_COUNT'] || MAX_THREAD_COUNT).to_i
end

def get_host_data(pings_allowed = false)
  signed_hosts.each do |certname, opts|
    hostname = cert_host_map[certname]
    count = current_thread_count(signed_hosts)
    if count < max_thread_limit
      logger.debug "New Thread started for #{certname}"
      # start up a new thread and store it in the opts hash
      opts[:thread] = Thread.new do
        ip_address = ip_address(hostname)
        alive = udp_pingable?(hostname) if pings_allowed && ip_address
        opts.merge!(resolvable: !!ip_address, alive: alive, ip: ip_address, certname: certname)
      end
    else
      # the last thread started should be the longest wait
      item, item_opts = signed_hosts.find_all { |_i, o| o.key?(:thread) }.last
      logger.debug "Waiting on #{item}"
      item_opts[:thread].join # wait for the thread to finish
      # now that we waited lets try again
      redo
    end
  end
end

# @return [String] - the ip address of the associated hostname, nil if the hostname is invalid
# @note the Resolv library will sometimes return the ip of the dns server!
#    which if this happens can give false positives
def ip_address(host)
  Resolv.getaddress(host)
rescue Resolv::ResolvError, ArgumentError
  nil
end

# @return [String] - the hostname associated by the ip returned via a reverse lookup
def name_from_ip(ip)
  Resolv.getname(ip)
rescue Resolv::ResolvError
  nil
end

# @return [Boolean] - true if the host responds to a ping
def udp_pingable?(host)
  logger.debug("Pinging #{host}")
  `ping #{host} -c 2 -t 5`
  $CHILD_STATUS.success?
end

def print_table(datahash)
  puts datahash.to_yaml.sub('---', '')
end

# @return [Hash] a collection of previously signed hosts that have been deemed dead
# @note this will collect the ip address and try and ping the host to determine if it is dead
def dead_hosts
  # populate the host data which starts
  # collection in parallel
  pings_allowed = agree('Do you want to ping nodes to determine if they are alive? ', default: false)
  get_host_data(pings_allowed)

  # Post processing and waiting for threads to finish
  signed_hosts.find_all do |hostname, node|
    node[:thread].join
    node.delete(:thread)
    # find all dead hosts
    !node[:alive] && (!protected_certs.include?(node[:certname]) || !protected_certs.include?(hostname))
  end.to_h
end

def get_nodes(type)
  nodes = case type.to_s
          when /deactivated/
            deactivated_nodes
          when /dead/
            dead_hosts.keys
          when /expired/
            expired_nodes
          else
            logger.info("#{type} is not valid, how did we get here?")
            []
          end
end
puts '--------------------------------------------'
puts "\nWelcome to the puppet node cleanup utility"
puts 'This script will aid in cleaning up dead hosts'
puts '--------------------------------------------'
check_dead_hosts = agree('Should we check for dead hosts without using puppetdb (experimental)? ', default: false)

puts
puts "PuppetDB Node TTL : #{node_ttl}"
puts "Deactivated Nodes : #{deactivated_nodes.count}"
puts "Expired Nodes     : #{expired_nodes.count}"
puts "Dead Hosts        : #{check_dead_hosts ? dead_hosts.count : 'N/A'}"
puts

type = choose do |menu|
  menu.shell = true
  menu.prompt = 'Please choose the type of nodes to search for? '
  menu.choice(:deactivated, text: 'Find and list all deactivated nodes')
  menu.choice(:expired, text: 'Find and list all expired nodes')
  menu.choice('dead (experimental)', text: 'Find and list all dead nodes without puppetdb')
end

nodes = get_nodes(type)
if nodes.count > 0
  say("\n<%= color('We found the following list of possible dead hosts, please review. ', YELLOW) %>\n")
  print_table(nodes)
  puts
  purge_hosts = agree("<%= color('Do you wish to start purging all nodes in the list? ', RED, BOLD) %>", default: false)
  if purge_hosts
    puts
    certain = agree("<%= color('Are you certain you want to purge all nodes in the list? ', RED, BOLD) %>", default: false)
    purge_nodes(nodes, type.eql?('dead (experimental)')) if certain
    puts
  end
else
  puts 'No nodes to purge, moving on.'
end
