#!/opt/puppetlabs/puppet/bin/ruby
# frozen_string_literal: true

# Author: Corey Osman
# Email: corey@nwops.io
# Date: 4/24/20
# Purpose: This script will go through puppetdb and find
#  all the nodes that have expired or been deactivated.
#  This script will prompt the user to purge each node
#  unless AUTO_PURGE is set to true.

# Requirements:  This script requires the puppet-agent to be installed
#                The presence of the puppet agent will provide the necessary
#                ruby libraries for this script to run.
# 
require 'puppet'
require 'json'
require 'puppet/face'
require 'highline/import'

MAX_THREAD_COUNT = 20
Puppet.initialize_settings # required, otherwise grabbing the settings doesn't work
CA_DIR = Puppet.settings[:cadir]
PE_LIB = '/opt/puppetlabs/puppet/modules/puppet_enterprise/lib'
$LOAD_PATH << PE_LIB unless $LOAD_PATH.include?(PE_LIB)
NODE_FACE = Puppet::Face[:node, :current]

# @return a logger object
def logger
  @logger ||= begin
    require 'logger'
    level = if ENV['ENABLE_LOGGER']
              Logger::DEBUG
            else
              Logger::INFO
            end
    @logger = Logger.new(STDERR)
    @logger.level = level
    @logger
  end
end

def puppetdb_query(query)
  out = "curl -s -X GET https://#{Puppet.settings[:server]}:8081/pdb/query/v4 \
  --tlsv1 \
  --cacert #{Puppet.settings[:cacert]} \
  --cert #{Puppet.settings[:hostcert]} \
  --key #{Puppet.settings[:hostprivkey]} \
  --data-urlencode \"query=#{query}\""
  `#{out}`
end

# @return [Hash] - a list of node certnames as a hash
def node_query(query)
  data = JSON.parse(puppetdb_query(query))
  data.each_with_object({}) do |node, list|
    list[node['certname']] = node['value']
  end
end

# @return [Hash] - a certname to hostname Hash map of all the nodes
def cert_host_map
  @cert_host_map ||= node_query("facts {name = 'fqdn' }")
end

def purge_node(node_certs)
  # The deactivate action should only be available on master nodes as it is
  # included in the puppetdb-terminus package.
  if Puppet::Interface.find_action(:node, :deactivate, :current)
    NODE_FACE.deactivate(*node_certs)
    NODE_FACE.clean(*node_certs)
  else
    raise 'This command cannot be run on an agent node. Please try again on a master node.'
  end
end

# @summary - purges all passed in nodes from puppetdb and their certs
# param nodes [Array] - one or more hosts to purge
def purge_nodes(nodes, validate_each = false)
  # make purging the wrong node painfully hard
  if validate_each
    nodes.each do |node|
      answer = agree("<%= color('Purge: #{node}(y/n)? ', RED, BOLD) %>", default: false)
      purge_node(node) if answer
    end
  else
    logger.warn('Purging all nodes in list')
    purge_node(nodes)
  end
end

# @return [Array] - an array of certs that should not be removed
# @note This is not a complete list and will need to find the other certs
#  that may be required.
def infra_nodes
  data = puppetdb_query("resources[parameters] { type = 'Class' and title = 'Puppet_enterprise'}")
  params = JSON.parse(data)[0]['parameters']
  params.fetch_values('puppetdb_database_host', 'certificate_authority_host', 'console_database_host',
                      'console_host', 'database_host', 'puppetdb_host', 'puppet_master_host',
                      'pcp_broker_host', 'orchestrator_database_host',
                      'inventory_database_host', 'ha_enabled_replicas').flatten.uniq
end

def protected_certs
  @protected_certs ||= ['console-cert'] + infra_nodes
end

# @return [Hash] hash of certnames
def deactivated_nodes
  node_query('nodes[certname] {deactivated is not null }')
end

# @return [Hash] hash of certnames
def expired_nodes
  node_query('nodes[certname] {expired is not null }')
end

# @return [String] - the value of the node ttl
# @note calls out to puppetdb and checks the node_ttl from the class resource
def node_ttl
  query = "resources[parameters] {type = 'Class' and title = 'Puppet_enterprise::Profile::Puppetdb' }"
  data = JSON.parse(puppetdb_query(query))
  data.first['parameters']['node_ttl']
end

def print_table(datahash)
  puts datahash.to_yaml.sub('---', '')
end

# @return [Array] - an array of node certs that are expired or deactivated
def get_nodes(type)
  nodes = case type.to_s
          when /deactivated/
            deactivated_nodes
          when /expired/
            expired_nodes
          else
            logger.info("#{type} is not valid, how did we get here?")
            []
          end
  nodes.keys - protected_certs
end

def requirements_check
  unless File.exist?(PE_LIB)
    say("\n<%= color('This puppet_enterprise module does not exist and it should, is this a master node?', RED) %>\n")
    exit 1
  end
  unless Puppet::Interface.find_action(:node, :deactivate, :current)
    say("\n<%= color('This command cannot be run on an agent node. Please try again on a master node.', RED) %>\n")
    exit 1
  end
end

requirements_check

puts '--------------------------------------------'
puts "\nWelcome to the puppet node cleanup utility"
puts 'This script will aid in cleaning up dead hosts'
puts 'by using puppetdb to discover expired or deactivated'
puts 'nodes.'
puts 'Set the env variable AUTO_PURGE=true to auto purge'
puts '--------------------------------------------'
puts
puts "PuppetDB Node TTL : #{node_ttl}"
puts "Deactivated Nodes : #{deactivated_nodes.count}"
puts "Expired Nodes     : #{expired_nodes.count}"
puts

type = choose do |menu|
  menu.header = 'Please choose the type of nodes to search for'
  menu.shell = true
  menu.prompt = '? '
  menu.choices(:deactivated, :expired)
end

nodes = get_nodes(type)
if nodes.count > 0
  say("\n<%= color('We found the following list of possible dead hosts, please review. ', YELLOW) %>\n")
  print_table(nodes)
  puts
  purge_hosts = agree("<%= color('Do you wish to start purging all nodes in the list? ', RED, BOLD) %>", default: false)
  purge_nodes(nodes, ENV['AUTO_PURGE'] !~ /true/)
else
  puts 'No nodes to purge, move along.'
end
